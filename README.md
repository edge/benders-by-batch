# Presentation

Repository containing a C++ implementation of the Benders by batch algorithm described in the article:
* Xavier Blanchot, François Clautiaux, Boris Detienne, Aurélien Froger, Manuel Ruiz. (2023). The Benders by batch algorithm: design and stabilization of an enhanced algorithm to solve multicut Benders reformulation of two-stage stochastic programs. _European Journal of Operational Research_. DOI: [10.1016/j.ejor.2023.01.004](https://doi.org/10.1016/j.ejor.2023.01.004) [postprint version accessible [here](https://hal.science/hal-03286135v4/document)]

Specifically, the code contains the following optimization algorithms for solving stochastic two-stage linear programs:
* the Benders by batch algorithm (with or without stabilization)
* a classic Benders decomposition algorithm (monocut, multicut or cut aggregation by batch of subproblems | with or without in-out stabilization)
* a level bundle algorithm (monocut)
* IBM ILOG CPLEX barrier algorithm 

# Licence and authors

This work is licensed under the Apache License, Version 2.0 (see the LICENSE file at the root of the project).

The code has been developed by Xavier Blanchot (from 2020 to 2022), Manuel Ruiz (from 2018 to 2019), and Aurélien Froger (2022-2023). Xavier Blanchot contributed to the implementation of all the algorithms. Manuel Ruiz contributed to the implementation of the classic Benders decomposition algorithm. Aurélien Froger helped make the code freely accessible (code cleanup, renaming, documentation). François Clautiaux, Boris Detienne, Aurélien Froger and Manuel Ruiz were scientifically supervising the PhD thesis of Xavier Blanchot, during which most of the code was produced.


# Input format

The problem data needs to be in the SMPS [[1]](#references) format. It should contain three files:
* The _core file_ fixes the problem dimensions and deterministic coefficients as well as the locations of all the stochastic coefficients (MPS format)
* The _time file_ describes the dynamic structure of the problem and breaks the data into the two stages. 
* The _stochastics file_ gives information about the stochastic data.

When the problem data is read for the first time, the program creates three additional files:
* _master.mps_ is the first-stage problem (i.e., the initial restricted master problem without the epigraph variables denoted \theta_s in the paper)
* _structure.txt_ links the first-stage variables between the first-stage and second-stage problems (specifically, it contains the index of the first-stage variables in the first-stage problem and in the subproblems)
* _subproblem_init.mps_ gives the "generic" formulation of the subproblem (random coefficients are later modified for each subproblem according to the corresponding scenario)

Note that having the file _master.mps_ without the other two files just described leads to a solver crash. In this case, delete the file _master.mps_ and let the solver regenerate the three files.
# Options

The options of the solver are given in a text file (.txt)

| **Option name**          | **Type** | **Default value** | **Description**                                                                                                                                                                                                                                                    |
|--------------------------|----------|-------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| TIME_LIMIT               | double   | -1                | Time limit to run the algorithm (-1 : no limit) / Note that the algorithm will finish the current iteration before stopping                                                                                                                                        |
| MAX_ITERATIONS           | int      | -1                | Number of iterations limit of the algorithm (-1 : no limit)                                                                                                                                                                                                        |
| GAP                      | double   | 1e-6              | Required optimality gap                                                                                                                                                                                                                                            |         
| GAP_TYPE                 | string   | ABSOLUTE          | Type of gap (RELATIVE or ABSOLUTE)                                                                                                                                                                                                                                 |                 
| DATA_FORMAT              | string   | SMPS              | only SMPS format is supported                                                                                                                                                                                                                                      |
| CORFILE_NAME             | string   | instance.cor      | name of the core file                                                                                                                                                                                                                                              |
| TIMEFILE_NAME            | string   | instance.tim      | name of the time file                                                                                                                                                                                                                                              |
| STOFILE_NAME             | string   | instance.sto      | name of the stoch file                                                                                                                                                                                                                                             |
| SEED                     | int      | -1                | Random seed to generate an instance (i.e., generating the subproblems) from a SMPS problem / -1 means that the seed is set according to the system time                                                                                                            | 
| ALGORITHM                | string   | BENDERS_BY_BATCH  | Algorithm to solve the two-stage stochastic linear program: BASE (Classic Benders decomposition algorithm) IN-OUT (Classic with in-out stabilization), LEVEL (level bundle), BENDERS_BY_BATCH (Benders By Batch)                                                   |
| SOLVER                   | string   | CPLEX             | Choice of the LP solver to use (only CPLEX supported)                                                                                                                                                                                                              |
| NUMERICAL_EMPHASIS       | bool     | false             | Activates numerical emphasis of CPLEX                                                                                                                                                                                                                              |
| MASTER_METHOD            | string   | SIMPLEX           | Method used to solve the master problem (SIMPLEX, BARRIER or BARRIER_WO_CROSSOVER)                                                                                                                                                                                 |
| SUBPROBLEM_NUMBER        | int      | -1                | Number of subproblems used to solve the problem (-1 for all subproblems present in the structure file)                                                                                                                                                             |
| SUBPROBLEM_WEIGHT        | string   | CONSTANT          | UNIFORM (1/n), CONSTANT (to set in SUBPROBLEM_WEIGHT_VALUE), or a txt file linking each subproblem to its weight                                                                                                                                                   |
| SUBPROBLEM_WEIGHT_VALUE  | double   | 1                 | If SUBPROBLEM_WEIGHT is CONSTANT, set here the divisor required                                                                                                                                                                                                    |
| THETA_LB                 | double   | -1e10             | Initial lower bound value on epigraph variables (Can change dramatically convergence rate)                                                                                                                                                                         |
| INIT_MEAN_VALUE_SOLUTION | bool     | true              | Initialization with the solution of the mean value problem                                                                                                                                                                                                         | 
| N_SHUFFLE_SUBPROBLEMS    | int      | 0                 | when ALGORITHM=BENDERS_BY_BATCH, it shuffles the subproblems this number of times before generating the batches (to test different random orders of the subproblems)                                                                                               |
| SORTING_METHOD           | string   | ORDERED           | Method (ORDERED, RANDOM) to sample subproblems at each iteration when ALGORITHM = BENDERS_BY_BATCH                                                                                                                                                                 |
| BATCH_SIZE               | int      | 1                 | Number of subproblems in each batch (not a percentage)                                                                                                                                                                                                             |
| CUT_AGGREGATION          | bool     | false             | if cuts generated from different subproblems should be aggregated                                                                                                                                                                                                  |
| CUT_AGGREGATION_LEVEL    | int      | 2                 | 1 = for each batch of subproblems, the cuts are aggregated into a single cut (CutAggr),  2 = all cuts are aggregated into a single cut (monocut)                                                                                                                   |
| STEP_SIZE                | float    | 1.0               | Value of the stabilization parameter: alpha\in(0,1]  when ALGORITHM=IN-OUT or BENDERS_BY_BATCH (\alpha = 1 means no stabilization), \lambda\in[0,1) when ALGORITHM=LEVEL                                                                                           |
| ALPHA_STRAT              | string   | DYNAMIC           | dynamic update of the stabilization parameter alpha (when ALGORITHM=IN-OUT) / every value other than DYNAMIC means that alpha is not updated                                                                                                                       |
| BETA                     | double   | 0.0               | Value of beta in memory stabilizations  (when ALGORITHM=BENDERS_BY_BATCH)                                                                                                                                                                                          | 
| MEMORY_TYPE              | string   | WITHOUT           | WITHOUT or SOLUTION  (when ALGORITHM=BENDERS_BY_BATCH)                                                                                                                                                                                                             |
| PRINT_OPTIONS            | bool     | true              | to print all the options at the beginning of the log                                                                                                                                                                                                               |
| PRINT_SOLUTION           | bool     | true              | True if the computed solution should be printed at the end of the algorithm                                                                                                                                                                                        |
| LOG_LEVEL                | int      | 3                 | level of detail of log output (from 1 to 3)                                                                                                                                                                                                                        |
| LOG_NUMBER_ITE           | int      | 1                 | To print the log every "k" iteration (only for BENDERS_BY_BATCH)                                                                                                                                                                                                   |
| TRACE                    | int      | 0                 | Set to 1 if output is wanted for the master, 2 for subproblems, 3 for both, 0 otherwise                                                                                                                                                                            |
| WRITE_ERRORED_PROB       | bool     | true              | True if a problem solved without an optimality status (e.g., infeasible, unbounded, ...) should be written in a file before exit                                                                                                                                   |
| WRITE_DETERMINISTIC_PRB  | bool     | false             | True if the deterministic reformulation of the problem should be written in a MPS file, whose name starts with the prefix "NSP" and contains the value of SUBPROBLEM_NUMBER and SEED (the barrier algorithm can then be run using the CPLEX Interactive Optimizer) |



# Installation

CMake 2.8 or later is required for compiling the C++ code.

IBM ILOG CPLEX is required for solving the mathematical models. An environment variable called CPLEX_STUDIO_DIR must point to the folder where the _'cplex'_ folder exists.

To compile the code: 
1. create a _'build'_ folder
2. launch the command `cmake ..` from this folder
3. launch the command `cmake --build . --target solverStoch2LP --config Release`

# Usage 

The solver has to be launched from the folder where CORFILE_NAME, TIMEFILE_NAME, and STOFILE_NAME exist.
* Unix: `path_to_executable/solverStoch2LP path_to_options_file`
* Windows: `path_to_executable/solverStoch2LP.exe path_to_options_file`


# Acknowledgments

This project has been funded by [RTE](https://www.rte-france.com/) (Réseau de Transport d’Electricité), the French company in charge of the electricity network management, through the projects Antares and [Antares Xpansion](https://github.com/AntaresSimulatorTeam/antares-xpansion).

We gratefully acknowledge the financial support of [Association nationale de la recherche et de la technologie (ANRT)](https://www.anrt.asso.fr/fr) for the PhD thesis of Xavier Blanchot.

We would also like to thank [Inria](https://www.inria.fr/en), [Institut de Mathématiques de Bordeaux](https://www.math.u-bordeaux.fr) (CNRS - UMR 5251) and [Université de Bordeaux](https://www.u-bordeaux.fr/en) for their support.

# References

[1] Gassmann, H. I., & Schweitzer, E. (2001). A comprehensive input format for stochastic linear programs. Annals of Operations Research, 104(1), 89-125. DOI:[10.1023/A:1013138919445](https://doi.org/10.1023/A:1013138919445)

	
