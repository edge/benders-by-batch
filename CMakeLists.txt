CMAKE_MINIMUM_REQUIRED(VERSION 2.8)
PROJECT(BendersByBatch)

SET(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR})
SET(LIBRARY_OUTPUT_PATH ${CMAKE_BINARY_DIR})

IF (WIN32)
    #	SET( CMAKE_CONFIGURATION_TYPES "Debug;Release;" CACHE STRING "limited configs" FORCE)
    #	SET( CMAKE_CXX_FLAGS_DEBUG          "/D_DEBUG /MTd /Zi /Ob0 /Od /RTC1" )
    #	SET( CMAKE_CXX_FLAGS_RELEASE        "/MT /O2 /Ob2 /D NDEBUG")
ELSE ()
    SET(CMAKE_BUILD_TYPE "RELEASE")
    #SET ( CMAKE_BUILD_TYPE "DEBUG"     )
    #	IF( WITH_DEBUG )
    #		SET ( CMAKE_BUILD_TYPE "DEBUG"     )
    #	ELSE( WITH_DEBUG )GENDIOR
    #		SET ( CMAKE_BUILD_TYPE "RELEASE"     )
    #	ENDIF( WITH_DEBUG )
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -DNDEBUG")
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -pedantic -fmessage-length=0 -fPIC")
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wno-conversion -Wno-sign-compare")
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-strict-aliasing -Wno-unused-parameter")
ENDIF (WIN32)

MESSAGE("CMAKE_BINARY_DIR : ${CMAKE_BINARY_DIR}")
MESSAGE("CMAKE_SOURCE_DIR : ${CMAKE_SOURCE_DIR}")
MESSAGE("CMAKE_C_COMPILER_VERSION : ${CMAKE_C_COMPILER_VERSION}")
MESSAGE("CMAKE_CXX_COMPILER_VERSION : ${CMAKE_CXX_COMPILER_VERSION}")

MESSAGE("CPLEX_STUDIO_DIR  is $ENV{CPLEX_STUDIO_DIR}")

INCLUDE_DIRECTORIES(./src_cpp)
INCLUDE_DIRECTORIES($ENV{CPLEX_STUDIO_DIR}/cplex/include/ilcplex)

FILE(
        GLOB
        FILES
        ./src_cpp/*.cc
        ./src_cpp/*.cpp
        ./src_cpp/*.h
        ./src_cpp/*.hxx
)

####################################################################
# LIBRARY : core
####################################################################
ADD_LIBRARY(core STATIC ${FILES})

####################################################################
# EXECUTABLE : solverStoch2LP
####################################################################
ADD_EXECUTABLE(solverStoch2LP exe_cpp/main.cpp)
TARGET_LINK_LIBRARIES(solverStoch2LP core)
IF (WIN32)
    find_library(CPLEX_LIBRARY_DEBUG
            NAMES cplex 12100 cplex2010 cplex2210
            PATHS "$ENV{CPLEX_STUDIO_DIR}/cplex/lib/*/stat_mdd")
    find_library(CPLEX_LIBRARY_OPTIMIZED
            cplex2010
            PATHS "$ENV{CPLEX_STUDIO_DIR}/cplex/lib/*/stat_mda")
    TARGET_LINK_LIBRARIES(solverStoch2LP
            debug ${CPLEX_LIBRARY_DEBUG}
            optimized ${CPLEX_LIBRARY_OPTIMIZED}
            )
ELSE ()
    set(THREADS_PREFER_PTHREAD_FLAG TRUE)
    find_package(Threads REQUIRED)
    find_library(CPLEX_LIBRARY
            cplex
            PATHS "$ENV{CPLEX_STUDIO_DIR}/cplex/lib/*/static_pic/")
    TARGET_LINK_LIBRARIES(solverStoch2LP ${CPLEX_LIBRARY} ${CMAKE_DL_LIBS})
ENDIF (WIN32)


