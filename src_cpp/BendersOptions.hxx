// Time limit for Benders decomposition ( -1 for no limit )
BENDERS_OPTIONS_MACRO(TIME_LIMIT, double, -1)

//Maximum number of iterations accepted
BENDERS_OPTIONS_MACRO(MAX_ITERATIONS, int, -1)

//Level of precision accepted 
BENDERS_OPTIONS_MACRO(GAP, double, 1e-6)

//Level of precision accepted 
BENDERS_OPTIONS_MACRO(GAP_TYPE, std::string, "ABSOLUTE")

// Data format : DECOMPOSED (one MPS by scenario) or SMPS
BENDERS_OPTIONS_MACRO(DATA_FORMAT, std::string, "SMPS")

// Name of .tim file if data format is SMPS
BENDERS_OPTIONS_MACRO(TIMEFILE_NAME, std::string, "instance.tim")

// Name of .cor file if data format is SMPS
BENDERS_OPTIONS_MACRO(CORFILE_NAME, std::string, "instance.cor")

// Name of .sto file if data format is SMPS
BENDERS_OPTIONS_MACRO(STOFILE_NAME, std::string, "instance.sto")

// Seed for std::rand
BENDERS_OPTIONS_MACRO(SEED, int, -1)

// Algorithm used to solve the problem (BASE, IN-OUT, BENDERS_BY_BATCH, LEVEL)
BENDERS_OPTIONS_MACRO(ALGORITHM, std::string, "BENDERS_BY_BATCH")

//Choice of the solver to use (XPRESS only for now)
BENDERS_OPTIONS_MACRO(SOLVER, std::string, "CPLEX")

// Set to 1 to activate numerical emphasis of solver, better precision but a little bit slower
BENDERS_OPTIONS_MACRO(NUMERICAL_EMPHASIS, bool, 0)

//Method use to solve the master problem (either SIMPLEX, BARRIER or BARRIER_WO_CROSSOVER)
BENDERS_OPTIONS_MACRO(MASTER_METHOD, std::string, "SIMPLEX")

//Number of subproblems to use to solve the problem
BENDERS_OPTIONS_MACRO(SUBPROBLEM_NUMBER, int, -1)

//UNIFORM (1/n), CONSTANT (to set in SUBPROBLEM_WEIGHT_VALUE), or a txt file linking each subproblem to its weight
BENDERS_OPTIONS_MACRO(SUBPROBLEM_WEIGHT, std::string, "UNIFORM")

//If SUBPROBLEM_WEIGHT is CONSTANT, set here the divisor required
BENDERS_OPTIONS_MACRO(SUBPROBLEM_WEIGHT_VALUE, double, 1)

// LB set on epigraph variables
BENDERS_OPTIONS_MACRO(THETA_LB, double, -1e10)

// BOOL to say if init algorithm with mean value problem solution
BENDERS_OPTIONS_MACRO(INIT_MEAN_VALUE_SOLUTION, bool, 1)

// TO SET THE INITIAL ORDER OF SUBPROBLEMS
BENDERS_OPTIONS_MACRO(N_SHUFFLE_SUBPROBLEMS, int, 0)

// Method to sample scenarios if ALGORITHM == BENDERS_BY_BATCH (ORDERED, RANDOM)
BENDERS_OPTIONS_MACRO(SORTING_METHOD, std::string, "ORDERED")

// Number of subproblems solved at each iteration on each machine if ALGORITHM == BENDERS_BY_BATCH
BENDERS_OPTIONS_MACRO(BATCH_SIZE, int, 1)

//True if cuts need to be aggregated (CutAggr or monocut), false otherwise
BENDERS_OPTIONS_MACRO(CUT_AGGREGATION, bool, false)

//Aggregation level:
// 1 = for each batch of subproblems, the cuts are aggregated into a single cut (CutAggr)
// 2 = all cuts are aggregated into a single cut (monocut)
BENDERS_OPTIONS_MACRO(CUT_AGGREGATION_LEVEL, int, 2)

// Step size of BENDERS_BY_BATCH algorithm (x(k) = x(k-1) + step_size* ( xMaster - x(k-1) )
BENDERS_OPTIONS_MACRO(STEP_SIZE, float, 1.0)

// Static or dynamic strategy in in-out separation scheme
BENDERS_OPTIONS_MACRO(ALPHA_STRAT, std::string, "DYNAMIC")

// memory parameter (0 without memory, beta in [0;1[)
BENDERS_OPTIONS_MACRO(BETA, double, 0.0)

// memory type stabilization (WITHOUT, SOLUTION, DIRECTION)
BENDERS_OPTIONS_MACRO(MEMORY_TYPE, std::string, "WITHOUT")

//True if similar cuts should be deleted, false otherwise
BENDERS_OPTIONS_MACRO(DELETE_CUT, bool, false)

// Bool to say if the options appear in the log
BENDERS_OPTIONS_MACRO(PRINT_OPTIONS, bool, 1)

// Bool to say if the optimal solution appear in the log
BENDERS_OPTIONS_MACRO(PRINT_SOLUTION, bool, 1)

//Determine the degree of detail of the output, from 1 to 3
BENDERS_OPTIONS_MACRO(LOG_LEVEL, int, 3)

// Iterations when printing a log line, only for BENDERS_BY_BATCH
BENDERS_OPTIONS_MACRO(LOG_NUMBER_ITE, int, 1)

//Set to 1 if output is wanted for the master, 2 for subproblems, 3 for both, 0 otherwise
BENDERS_OPTIONS_MACRO(TRACE, int, 0)

//Bool to say if non optimal problem should be written in a file before exit
BENDERS_OPTIONS_MACRO(WRITE_ERRORED_PROB, bool, true)

// To write the deterministic reformulation
BENDERS_OPTIONS_MACRO(WRITE_DETERMINISTIC_PRB, bool, 0)

//Path to the folder where input files are stored
BENDERS_OPTIONS_MACRO(INPUTROOT, std::string, ".")

//Name of the master problem file, if different from 'master' 
BENDERS_OPTIONS_MACRO(MASTER_NAME, std::string, "master")

//Number of subproblems to use to solve the problem
BENDERS_OPTIONS_MACRO(STRUCTURE_FILE, std::string, "structure.txt")

//Path to the folder where output files should be printed
BENDERS_OPTIONS_MACRO(OUTPUTROOT, std::string, ".")

// The tolerance for detecting that the current solution to the RMP has been cut
BENDERS_OPTIONS_MACRO(CUT_MASTER_TOL, double, -1e-1)