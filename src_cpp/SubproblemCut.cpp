#include "SubproblemCut.h"

/*!
*  \brief Constructor of a subproblem cut handler from subproblem cut data
*
*  \param data : pointer to a subproblem cut data
*/
SubproblemCutDataHandler::SubproblemCutDataHandler(SubproblemCutDataPtr & data) : _data(data) {
	get_int().resize(SubproblemCutInt::MAXINT_VALUE);
	get_dbl().resize(SubproblemCutDbl::MAXDBL_VALUE);
	get_str().resize(SubproblemCutDbl::MAXDBL_VALUE);
}

SubproblemCutDataHandler::~SubproblemCutDataHandler() {

}

/*!
*  \brief Get subgradient of a subproblem cut
*/
Point & SubproblemCutDataHandler::get_subgradient() {
	return _data->first.first.first;
}

/*!
*  \brief Get int variable of a subproblem cut
*/
IntVector & SubproblemCutDataHandler::get_int() {
	return _data->first.first.second;
}

/*!
*  \brief Get double variable of a subproblem cut
*/
DblVector & SubproblemCutDataHandler::get_dbl() {
	return _data->first.second;
}

/*!
*  \brief Get string variable of a subproblem cut
*/
StrVector & SubproblemCutDataHandler::get_str() {
	return _data->second;
}

/*!
*  \brief Get int variable of a subproblem cut (SIMPLEXITE, LPSTATUS, HAS_CUT)
*/
int & SubproblemCutDataHandler::get_int(SubproblemCutInt key) {
	return get_int()[key];
}

/*!
*  \brief Get double variable of a subproblem cut (SUBPROBLEMCOST, ALPHA_I, SUBPROBLEM_TIMER)
*/
double & SubproblemCutDataHandler::get_dbl(SubproblemCutDbl  key) {
	return get_dbl()[key];
}

/*!
*  \brief Get string variable of a subproblem cut
*/
std::string & SubproblemCutDataHandler::get_str(SubproblemCutStr key) {
	return get_str()[key];
}

/*!
*  \brief Get subgradient of a subproblem cut
*/
Point const & SubproblemCutDataHandler::get_subgradient() const {
	return _data->first.first.first;
}

/*!
*  \brief Get int variable of a subproblem cut
*/
IntVector const & SubproblemCutDataHandler::get_int() const {
	return _data->first.first.second;
}

/*!
*  \brief Get double variable of a subproblem cut
*/
DblVector const & SubproblemCutDataHandler::get_dbl() const {
	return _data->first.second;
}

/*!
*  \brief Get string variable of a subproblem cut
*/
StrVector const & SubproblemCutDataHandler::get_str() const {
	return _data->second;
}

/*!
*  \brief Get int variable of a subproblem cut (SIMPLEXITE, LPSTATUS)
*/
int SubproblemCutDataHandler::get_int(SubproblemCutInt key)const {
	return get_int()[key];
}

/*!
*  \brief Get double variable of a subproblem cut (SUBPROBLEMCOST, ALPHA_I, SUBPROBLEM_TIMER)
*/
double SubproblemCutDataHandler::get_dbl(SubproblemCutDbl  key) const {
	return get_dbl()[key];
}

/*!
*  \brief Get string variable of a subproblem cut
*/
std::string const & SubproblemCutDataHandler::get_str(SubproblemCutStr key) const {
	return get_str()[key];
}

/*!
*  \brief Comparator overloading of subproblem cut trimmer
*/
bool SubproblemCutTrimmer::operator<(SubproblemCutTrimmer const & other) const {
	Predicate point_comp;
	if (std::fabs(_const_cut - other._const_cut) < EPSILON_PREDICATE) {
		return point_comp(_data_cut->get_subgradient(), other._data_cut->get_subgradient());
	}
	else {
		return (_const_cut < other._const_cut);
	}
}

/*!
*  \brief Constructor of Subproblem cut trimmer from an handler and trial values
*/
SubproblemCutTrimmer::SubproblemCutTrimmer(SubproblemCutDataHandlerPtr & data, Point & x0) : _data_cut(data), _x0(x0) {
	_const_cut = _data_cut->get_dbl(SUBPROBLEM_COST);

	for (auto const & kvp : _x0) {
		_const_cut -= get_subgradient().find(kvp.first)->second * kvp.second;
	}
}

/*!
*  \brief Get subgradient of a subproblem cut trimmer
*/
Point const & SubproblemCutTrimmer::get_subgradient() const {
	return _data_cut->get_subgradient();
}

/*!
*  \brief Operator overloading of subproblem cut trimmer for stream output
*
*  \param stream : output stream
*/
std::ostream & operator<<(std::ostream & stream, SubproblemCutTrimmer const & rhs) {
	rhs.print(stream);
	return stream;
}

/*!
*  \brief Function to print a subproblem cut trimmer
*
*  \param stream : output stream
*/
void SubproblemCutTrimmer::print(std::ostream & stream)const {
	std::stringstream buffer;
	buffer << _const_cut << get_subgradient();
	stream << buffer.str();
}

/*!
*  \brief Stream output overloading of subproblem cut handler
*
*  \param stream : output stream
*/
std::ostream & operator<<(std::ostream & stream, SubproblemCutDataHandler const & rhs) {
	rhs.print(stream);
	return stream;
}

/*!
*  \brief Function to print a subproblem cut handler
*
*  \param stream : output stream
*/
void SubproblemCutDataHandler::print(std::ostream & stream)const {
	std::stringstream buffer;
	buffer << get_dbl(SUBPROBLEM_COST) << get_subgradient();
	stream << buffer.str();
	stream << " Simplexiter " << get_int(SIMPLEXITER) << " | ";
}

/*!
*  \brief Stream output overloading of a subproblem cut
*
*  \param stream : output stream
*/
std::ostream & operator<<(std::ostream & stream, SubproblemCutData const & rhs) {
	std::stringstream buffer;
	buffer << rhs.first.second[SUBPROBLEM_COST] << rhs.first.first.first;
	stream << buffer.str();
	stream << " Simplexiter " << rhs.first.first.second[SIMPLEXITER] << " | ";
	return stream;
}
