#pragma once

#include "Worker.h"
#include "SubproblemCut.h"


/*! 
* \class WorkerSubproblem
* \brief Class daughter of Worker Class, build and manage a subproblem
*/
class WorkerSubproblem;
typedef std::shared_ptr<WorkerSubproblem> WorkerSubproblemPtr;
typedef std::vector<WorkerSubproblemPtr> WorkerSubproblems;
typedef std::map<std::string, WorkerSubproblemPtr> SubproblemsMapPtr;


class WorkerSubproblem : public Worker {
public :
	double _proba;

public:

	WorkerSubproblem();
	WorkerSubproblem(Str2Int const & variable_map, std::string const & path_to_mps,
                     double const & SUBPROBLEM_WEIGHT, BendersOptions const & options);
	WorkerSubproblem(Str2Int const& variable_map, std::string const& path_to_mps,
                     double const& SUBPROBLEM_WEIGHT, BendersOptions const& options, StrPairVector keys,
                     DblVector values, WorkerPtr fictif);
	virtual ~WorkerSubproblem();

	void set_realisation_to_prob(StrPairVector keys, DblVector values);

public:
	void write(int it);
	void fix_to(Point const & x0);
	void get_subgradient(Point & s);

public :
	// Surcharge avec multiplication par _proba
	void get_value(double& lb);
};


