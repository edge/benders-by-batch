#pragma once 

#include "common.h"

/*!
* \class BendersOptions
* \brief Class containing the options
*/
class BendersOptions {
public:

#define BENDERS_OPTIONS_MACRO(name__, type__, default__) type__ name__;
#include "BendersOptions.hxx"
#undef BENDERS_OPTIONS_MACRO

	BendersOptions();

	void read(std::string const & file_name);
	void print(std::ostream  & stream)const;

	void write_default();

	std::string get_master_path() const;
	std::string get_structure_path() const;
	std::string get_subproblem_path(std::string const & subproblem_name) const;

	double subproblem_weight(int nsubproblems, std::string const &name)const;

	Str2Dbl _weights;
};


